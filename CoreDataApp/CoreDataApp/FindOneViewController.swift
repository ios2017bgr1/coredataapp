//
//  FindOneViewController.swift
//  CoreDataApp
//
//  Created by Sebas on 30/1/18.
//  Copyright © 2018 Sebas. All rights reserved.
//

import UIKit
import CoreData

class FindOneViewController: UIViewController {

    @IBOutlet weak var addresLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var person:Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = person?.name!
        addresLabel.text = person?.address!
        phoneLabel.text = person?.phone!

    }

    @IBAction func deleteButtonPressed(_ sender: Any) {
        
        let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        manageObjectContext.delete(person!)
        
        do{
            try manageObjectContext.save()
        } catch {
            print("Error deleting object")
        }

        
    }
    

}
